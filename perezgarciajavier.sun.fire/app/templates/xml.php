<?xml version="1.0" encoding="UTF-8"?>
<catalog>
<?php foreach ($books as $b) { ?>
  <book id="<?php echo $b['id']; ?>">
    <author><?php echo $b['author']; ?></author>
    <title><?php echo $b['title']; ?></title>
    <genre><?php echo $b['genre']; ?></genre>
    <price><?php echo $b['price']; ?></price>
    <publish_date><?php echo $b['publish_date']; ?></publish_date>
    <description><?php echo $b['description']; ?></description>
  </book>
<?php } ?>
</catalog>
