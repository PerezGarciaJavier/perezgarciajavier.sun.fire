<?php

$app->get('/xml', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'municipio' => array(
      array(
        'id_municipio' => "1",
        'clave_municipio' => "000",
		'nombre_municipio' => "nacional",
		'id_entidad' => "1" )));
  $app->render('municipio.php', $datos);

});
